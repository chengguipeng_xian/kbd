Name:           kbd
Version:        2.2.0
Release:        1
Summary:        Tools for managing Linux console(keyboard, virtual terminals, etc.)
License:        GPLv2+
URL:            https://www.kbd-project.org/
                                                                                    
Source0:        http://ftp.altlinux.org/pub/people/legion/%{name}/%{name}-%{version}.tar.xz
Source1:        kbd-latsun-fonts.tar.bz2
Source2:        kbd-latarcyrheb-32.tar.bz2
Source3:        xml2lst.pl
Source4:        vlock.pamd
Source5:        kbdinfo.1
Source6:        cz-map.patch

Patch0:         kbd-1.15-keycodes-man.patch
Patch1:         kbd-1.15-sparc.patch
Patch2:         kbd-1.15-unicode_start.patch
Patch3:         kbd-1.15.3-dumpkeys-man.patch
Patch4:         kbd-1.15.5-sg-decimal-separator.patch
Patch5:         kbd-1.15.5-loadkeys-search-path.patch
Patch6:         kbd-2.0.2-unicode-start-font.patch
Patch7:         kbd-2.0.4-covscan-fixes.patch
Patch8:         kbd-2.2.0-fix-flags.patch
Patch9:         kbd-2.2.0-format-security.patch

BuildRequires:  bison flex gettext pam-devel check-devel
BuildRequires:  gcc console-setup xkeyboard-config automake 
Requires:       %{name}-misc = %{version}-%{release}
Requires:       %{name}-legacy = %{version}-%{release}
Provides:       vlock = %{version}
Conflicts:      vlock <= 1.3

%description
The %{name} project contains tools for managing Linux console ,including 
Linux console,virtual terminals, keyboard, etc, mainly, what they do 
is loading console fonts and keyboard maps.

%package 	misc
Summary:        Data for %{name} package
BuildArch:      noarch
 
%description 	misc
The %{name}-misc package contains data for %{name} package,including console fonts,
keymaps and so on. But %{name}-misc can do nothing without %{name}.

%package 	legacy
Summary:        Legacy data for %{name} package
BuildArch:      noarch
 
%description 	legacy
The %{name}-legacy package contains original keymaps for %{name} package.
But %{name}-legacy can do nothing without %{name}.

%package_help

%prep
%setup -q -a 1 -a 2
cp -fp %{SOURCE3} .
cp -fp %{SOURCE6} .
%patch0 -p1 -b .keycodes-man
%patch1 -p1 -b .sparc
%patch2 -p1 -b .unicode_start
%patch3 -p1 -b .dumpkeys-man
%patch4 -p1 -b .sg-decimal-separator
%patch5 -p1 -b .loadkeys-search-path
%patch6 -p1 -b .unicode-start-font
%patch7 -p1 -b .covscan-fixes.patch
%patch8 -p1 -b .fix-flags.patch
%patch9 -p1 -b .format-security.patch
autoconf

pushd data/keymaps/i386
cp qwerty/pt-latin9.map qwerty/pt.map
cp qwerty/sv-latin1.map qwerty/se-latin1.map

mv azerty/fr.map azerty/fr-old.map
cp azerty/fr-latin9.map azerty/fr.map

cp azerty/fr-latin9.map azerty/fr-latin0.map 

mv fgGIod/trf.map fgGIod/trf-fgGIod.map
mv olpc/es.map olpc/es-olpc.map
mv olpc/pt.map olpc/pt-olpc.map
mv qwerty/cz.map qwerty/cz-qwerty.map
popd

pushd po
rm -f gr.po gr.gmo
popd

iconv -f iso-8859-1 -t utf-8 < "ChangeLog" > "ChangeLog_"
mv "ChangeLog_" "ChangeLog"

%build
%configure --prefix=%{_prefix} --datadir=/lib/%{name} --mandir=%{_mandir} --localedir=%{_datadir}/locale --enable-nls
%make_build

%install
%make_install

rm -f %{buildroot}/lib/%{name}/keymaps/i386/qwerty/ro_win.map.gz

ln -s sr-cy.map.gz %{buildroot}/lib/%{name}/keymaps/i386/qwerty/sr-latin.map.gz

ln -s us.map.gz %{buildroot}/lib/%{name}/keymaps/i386/qwerty/ko.map.gz

mkdir -p %{buildroot}/bin
for binary in setfont dumpkeys %{name}_mode unicode_start unicode_stop loadkeys ; do
  mv %{buildroot}%{_bindir}/$binary %{buildroot}/bin/
done

sed -i -e 's,\<%{name}_mode\>,/bin/%{name}_mode,g;s,\<setfont\>,/bin/setfont,g' \
        %{buildroot}/bin/unicode_start

gzip -c %SOURCE5 > %{buildroot}/%{_mandir}/man1/kbdinfo.1.gz

cp -r %{buildroot}/lib/%{name}/locale/ %{buildroot}%{_datadir}/locale
rm -rf %{buildroot}/lib/%{name}/locale

mkdir -p %{buildroot}%{_sysconfdir}/pam.d
install -m 644 %{SOURCE4} %{buildroot}%{_sysconfdir}/pam.d/vlock

mkdir -p %{buildroot}/lib/%{name}/keymaps/legacy
mv %{buildroot}/lib/%{name}/keymaps/{amiga,atari,i386,include,mac,ppc,sun} %{buildroot}/lib/%{name}/keymaps/legacy

mkdir -p %{buildroot}/lib/%{name}/keymaps/xkb
perl xml2lst.pl < /usr/share/X11/xkb/rules/base.xml > layouts-variants.lst
while read line; do
  XKBLAYOUT=`echo "$line" | cut -d " " -f 1`
  echo "$XKBLAYOUT" >> layouts-list.lst
  XKBVARIANT=`echo "$line" | cut -d " " -f 2`
  ckbcomp "$XKBLAYOUT" "$XKBVARIANT" | gzip > %{buildroot}/lib/%{name}/keymaps/xkb/"$XKBLAYOUT"-"$XKBVARIANT".map.gz
done < layouts-variants.lst

cat layouts-list.lst | sort -u >> layouts-list-uniq.lst
while read line; do
  ckbcomp "$line" | gzip > %{buildroot}/lib/%{name}/keymaps/xkb/"$line".map.gz
done < layouts-list-uniq.lst

gunzip %{buildroot}/lib/%{name}/keymaps/xkb/fi.map.gz
mv %{buildroot}/lib/%{name}/keymaps/xkb/fi.map %{buildroot}/lib/%{name}/keymaps/xkb/fi-kotoistus.map
gzip %{buildroot}/lib/%{name}/keymaps/xkb/fi-kotoistus.map

gunzip %{buildroot}/lib/%{name}/keymaps/xkb/cz.map.gz
patch %{buildroot}/lib/%{name}/keymaps/xkb/cz.map < %{SOURCE6}
gzip %{buildroot}/lib/%{name}/keymaps/xkb/cz.map

%files 
%defattr(-,root,root)
%license COPYING
%doc AUTHORS 
/bin/*
%{_datadir}/locale/*
%{_bindir}/*
%{_mandir}/*/*
%config(noreplace) %{_sysconfdir}/pam.d/vlock

%files misc
%defattr(-,root,root)
/lib/%{name}/*
%exclude /lib/%{name}/keymaps/legacy

%files legacy
%defattr(-,root,root)
/lib/%{name}/keymaps/legacy/*

%files help
%defattr(-,root,root)
%doc ChangeLog README docs/doc/%{name}.FAQ*.html docs/doc/font-formats/*.html docs/doc/utf/utf* docs/doc/dvorak/*
%{_mandir}/man1/*.1.gz
%{_mandir}/man5/keymaps.5.gz
%{_mandir}/man8/*.8.gz

%changelog
* Tue Jul 28 2020 hanhui <hanhui15@huawei.com> - 2.2.0-1
- update to 2.2.0

* Wed Jan 22 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.0.4-11
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:bugfix in build process

* Wed Sep 18 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.0.4-10
- Package init
